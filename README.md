# PiKern
## Automate cross-compiling the Raspberry Pi patched kernel

The project uses YAML. Install the module and run the script.

You will need to install the following perl libraries:
- File::Copy::Recursive
- YAML

The following non-perl libraries/program are also required:
- pv
- git

On Gentoo you can install this with
`emerge -av dev-perl/YAML-LibYAML dev-perl/File-Copy-Recursive dev-vcs/git sys-apps/pv`

The script will automatically generate a YAML configuration at '''/usr/local/etc/pikern.conf'''

## Workflow
After editing your `pikern.conf`, you should copy the `freshen` script to the directory which was set
as `Base_Directory` in `pikern.conf` if you want to manage linux source branch selection.

### Freshen Script
`freshen` is a bash script that will show you the latest 5 branches from the raspberrypi/linux github
page. You will then be prompted to pick one of the branches and it will run:
`git clone -b "${branch}" --depth=1 git@github.com:/raspberrypi/linux.git`. If you haven't configured
ssh keys with git, you should edit `GIT_LINK` to use git https. e.g.
`https://github.com/raspberrypi/linux`. You may alternatively manually clone the branch you want to use.

If you want the freshen script to generate a defconfig, edit `DEFCONFIG` to `true`. I suggest leaving
it as false if you want to re-use a previously generated .config file.

If you're using pikern as a part of automated building, I would suggest implementing a similar script
into your system which handles pulling in the linux kernel sources instead of relying upon the freshen
script.

### Kernel Config Files
After pulling in the linux sources, you'll likely also want to create a "config" directory in
`Base_Directory`. This directory will store .config's generated for you to use in subsequent builds of
the linux kernel.

If you want to use a .config from a prior install, you need to rename it to `config` within the "config"
directory. For example, your config directory contains `config-6.9.1`. To use this in your 6.9.2 build,
simply `cp config-6.9.1 config`. The pikern script will then copy `config` to the linux directory as
`.config`.

If you're using a defconfig, you don't need to do anything. Pikern will detect that there is a .config
already and won't manage it.

### Running pikern.pl
Now it's time to compile the kernel. You simply need to run, './pikern.pl'. If all goes to plan, you
should now have a tarball (.tar.gz) file located at, `Images_Directory`
(default: /home/user/gentoo/rpikern/rpi_images).

## Using the generated image
The kernel + modules were organised to resemble a minimal filesytem. This allows you to extract the
.tar.gz on the root of a file system.

Here's an example of the file structure:

```
└── .
    ├── boot
    │   └── overlays
    └── usr
        ├── lib
        │   └── modules
        │       └── 6.9.1+
...
        │                       └── misc
        └── src
```

### Installation
To unpack the .tar.gz file, first go to the root of your sd card (or copy the .tar.gz to the root of
your raspberry pi using a utility such as scp), then run as root:

```
tar --no-overwrite-dir --no-same-owner -xzf linux-your.version.tar.gz
```

The above command ensures that you aren't overwriting directories. This is an issue as it will change
the permissions on /boot and /usr which will lead to some odd issues occurring. The --no-same-owner
flag is because fat32 /boot partition won't allow you to unpack respective files to it since you
likely compiled your kernel not as root.

You can verify if you upacked the modules by looking in `/usr/lib/modules`. Example output
looks like below:

```
user@rpi ~ $ ls -l /usr/lib/modules
total 8
drwxr-xr-x 4 root root 4096 May 22 05:17 6.8.10+
drwxr-xr-x 4 root root 4096 May 26 22:37 6.9.1+
```

The `kernel.img` and associated files were put in the /boot directory. They had overwrote the previous
version with the same file name. You can verify that using the `ls -l` command like above.

I would finally suggest removing old modules from /usr/lib/modules and removing old boot folder files
such as `config-old.version.number` as well as `symvers-old.version.number`.

## Post-install
If you're using a Gentoo system, you will likely want to make sure that your `/usr/src/linux` file is
symlinked correctly to the headers directory which pikern organised for us. This is achieved by running:

```
ln -s /lib/modules/6.9.1+/build /usr/src/linux
```
Some Gentoo ebuild also like to verify that certain modules are enabled/builtin and providing at least
the headers; which the headers provide most of this functionality.

The reason for using the headers instead of a full kernel source tree is because of the size difference.

## Final remarks
Finally reboot and enjoy your new kernel :)

## The ebuild related files
If you're using Gentoo, you can attempt to use the ebuild related template and ebuild.pl. This is here
primarily for my personal use. I personally wouldn't recommend using it however as it's still using
repoman instead of pkgdev.

Pkgdev has been the main recommended tool for at least 3 years now for quality ensurance with ebuilds.
One day I'll maybe fix the automated ebuild writing feature.
