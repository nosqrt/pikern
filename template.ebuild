# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Pre-compiled Raspberry Pi kernel, modules and headers for BCM2835 devices"
HOMEPAGE="http://10.0.0.160"
SRC_URI="arm? ( ${HOMEPAGE}/rpi_images/linux-${PV}-rpi.tar.gz -> rpi-sources-bin-${PV}.tar.gz )"

RESTRICT="mirror strip"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm"
IUSE="symlink"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}

# This is a binary package. Portage should 'skip' these steps.
src_configure() { :; }
src_compile() { :; }

src_install() {
	dodir "/boot"
	dodir "/lib/modules"
	mv "boot" "${ED}" || die
	mv "${S}/lib/modules/${PV}+" "${ED}/lib/modules" || die
}

pkg_postinst() {
	if use "symlink" ; then
		einfo "Creating symlink /lib/modules/${PV}+/build -> /usr/src/linux"
		[ -e /usr/src/linux ] && rm /usr/src/linux
		ln -s /lib/modules/${PV}+/build /usr/src/linux
	fi
}
