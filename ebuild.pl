#!/usr/bin/env perl

use strict;
use warnings;
use File::Copy qw(copy);
use YAML::XS;

sub LoadConfig($)
{
# Grab the YAML config and store it as a perl hash.
    return YAML::XS::LoadFile($_[0]);
}

sub ImagePath($)
{
# Grab the 'Images' path from the YAML config.
    return "$_[0]->{'Working_Directories'}->{'Base_Directory'}$_[0]->{'Working_Directories'}->{'Images_Directory'}";
}

sub ReadImage($)
{
# Only return the package version of each tarball.
    opendir(my $dir, shift);
    my @image =
        sort { $a cmp $b }
        map  { /^linux\-([\d|\.]+)\-rpi\.tar\.gz$/ ; $1 }
        grep { /^linux.*/ }
        readdir($dir);
    closedir($dir);

    return @image;
}

sub GitSubmit($)
{
}

sub Execute($)
{
    my @image = ReadImage(ImagePath(LoadConfig(shift)));

    return sub($)
    {
        my $ebuild = shift;

        foreach (glob("$ebuild/*")) { unlink($_) };
        foreach (@image) { copy("template.ebuild", "$ebuild/rpi-sources-bin-$_.ebuild") };
        copy("metadata.xml", "$ebuild/metadata.xml");

        return sub($)
        {
            my $distdir = shift;
            my @command = (
                "repoman manifest -f",
                "git add -- .",
                "git commit . -m 'Automated version bump @image'",
            );

            foreach (glob("$distdir/*")) { unlink($_) };
            local $ENV{'DISTDIR'} = $distdir;
            chdir("$ebuild");
            foreach (@command) { system("$_") };

            return sub()
            {
                chdir("$ebuild");
                system("repoman full");
                system("git push origin master");
            }
        }
    }
}

my $home = '/home/dani/snippets/git/localgit/brushy/sys-kernel/rpi-sources-bin';
my $conf = '/usr/local/etc/pikern.conf';
my $execute = ((Execute($conf))->($home))->('/home/dani/distdir');
#my $digest  = $execute->($home);
#my $git     = $digest->('/home/dani/distdir');
#$git->();
