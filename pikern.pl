#!/usr/bin/env perl
#
####~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~####
#                                                                      #
#                Copyright Daniella Kicsak,2019-2024                   #
#                                                                      #
####~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~####
#                                                                      #
#    PiKern                                                            #
# This script intends to make the process of cross-compiling a         #
# raspberry pi kernel a little easier by compiling and packaging it    #
# through a simple YAML configuration.                                 #
#                                                                      #
# The script intends to run a few passes over the kernel source and    #
# make it ready for distribution.                                      #
#                                                                      #
#    1. Make sure there is a `linux` file (or named appropriately as   #
#       configured in the config).                                     #
#    2. Ensure there is a kernel .config file ready and present, try   #
#       to grab one if not available.                                  #
#        - Is it possible to use an old config file and just roll it   #
#          forward?                                                    #
#    3. Use the config file to find where the crosscompilation tools   #
#       currently reside. Also grab the extra flags needed for the     #
#       compile time arguments (e.g. -j5).                             #
#    4. Create the command line arguments and execute them.            #
#    5. Compile the kernel modules into ../temp                        #
#    6. Package the kernel into the ../temp directory so it emulates   #
#       the file structure of a gentoo system (kernel source under     #
#       /usr/src).                                                     #
#    7. Make a copy of the kernel source and remove unecessary cruft   #
#       from it. (.git)                                                #
#    8. Package the kernel + modules appropriately, ready for          #
#       distribution.                                                  #
#    9. Clean up the kernel and temp directory so that it's ready to   #
#       run this script again.                                         #
####~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~####

use strict;
use warnings;
use File::Copy;
use File::Copy::Recursive qw(fcopy rcopy dircopy);
use File::Find;
use File::Path;
use YAML::XS;

### ~~~
# YAML loading and setup
### ~~~
sub Load_Config
{
    # Reads the YAML file and stores it memory as a perl hash.
    # If there isn't a config file already, a default file is created and that is used.
    my $confdir = shift;
    my $conf    = $confdir . shift;

    # Check if we have a configuration directory. Create one and fill with a default conf.
    ( !-d $confdir ) and mkpath( [$confdir], 0, 0744 );
    ( !-e $conf )    and &Init_Config($conf);

    # Converts the config YAML file to a perl readable hash.
    return YAML::XS::LoadFile($conf);
}

sub Init_Config
{
    # Generates a default configuration file if one doesn't already exist.
    ## Expects config file path.
    my $generate = {
                     'Compiler_Options' => {
                                             'Compiler_Location' => '/usr/bin/armv6j-unknown-linux-gnueabihf-',
                                             'Compiler_Jobs'     => '9',
                                             'Compiler_Arch'     => 'arm'
                     },
                     'Compile_Time_Options' => {
                                                 'Oldconfig'       => 'No',
                                                 'Menuconfig'      => 'Yes',
                                                 'Config_Location' => 'config/config',
                                                 'Kernel'          => 'Yes'
                     },
                     'Working_Directories' => {
                                                'Base_Directory'   => '/home/user/gentoo/rpikern/',
                                                'Linux_Directory'  => 'linux',
                                                'Temp_Directory'   => 'temp',
                                                'Images_Directory' => 'rpi_images',
                     },
                     'Packaging_Options' => {
                                              'Install_Boot'          => 'Yes',
                                              'Install_Config'        => 'Yes',
                                              'Install_Modules'       => 'Yes',
                                              'Install_Kernel'        => 'Yes',
                                              'Install_Kernel_Source' => 'No',
                                              'Install_DTB'           => 'Yes',
                                              'Install_DTS_Overlay'   => 'Yes',
                                              'Install_Syms'          => 'Yes',
                                              'Install_Headers'       => 'Yes',
                                              'Tar_Directory'         => 'Yes'
                     },
                     'Cleanup_Options' => {
                                            'Cleanup_Linux' => 'Yes',
                                            'Cleanup_Env'   => 'Yes'
                     }
    };

    # Encode the hash to YAML.
    if ( YAML::XS::DumpFile( $_[0], $generate ) ) {
        print("New config file created at $_[0]\nIt is advised to edit before continuing\n");
    }

}

### ~~~
# Envrionment checking
### ~~~
sub Check_Build_Env
{
    # Make sure that all of the build folders exist. If they don't create them and `cd` into there.
    ## EXPECTS: Reference to YAML hash and Internal hash.
    my $cc = $_[0]->{'Compiler_Options'}->{'Compiler_Location'} . 'gcc';

    # See if our build env has all the necessary directories.
    Create_Build_Env( $_[1]->{'Base_Path'},   $_[1]->{'Linux_Path'}, $_[1]->{'Image_Path'},
                      $_[1]->{'Module_Path'}, $_[1]->{'Boot_Path'},  $_[1]->{'Source_Path'} );

    # Check to make sure the Linux sources contain the necessary build files.
    ( ( !-e "$_[1]->{'Linux_Path'}/Makefile" ) && ( !-e "$_[1]->{'Linux_Path'}/Kconfig" ) )
      and Download_Sources();

    # Check that the cross compiler exists.
    ( !-e $cc )
      and die("The cross-compiler: $cc doesn't exist! Please correct your configuration file.");

    # Make sure that there's a valid config file. Use a defconfig from linux/arch/arm/configs if none are found.
    if ( -e $_[1]->{'Config_Path'} ) {
        copy( $_[1]->{'Config_Path'}, "$_[1]->{'Linux_Path'}/.config" );
        print("Using Linux config file: $_[1]->{'Config_Path'}\n\tThis was set in your configuration.\n\n");
    }
    elsif ( -e "$_[1]->{'Linux_Path'}/.config" ) {
        print( "Using old Linux config file: $_[1]->{'Linux_Path'}/.config\n\t" . "This was found in your linux directory.\n\n" );
    }
    else {
        my $defConf = $_[1]->{'Linux_Path'} . "/arch/arm/configs/bcmrpi_defconfig";
        copy( $defConf, "$_[1]->{'Linux_Path'}/.config" );
        print("Using found Linux config file: $defConf\n\tThis is a generic config.\n\n");
    }

}

sub Create_Build_Env
{
    # Checks if our build env has all the directories we have set in the configuration. Create then with 740 perms
    # if they don't exist..
    ## EXPECTS: Array of directory paths.
    foreach (@_) { ( !-d $_ ) and mkpath( [$_], 0, 0740 ); }
}

sub Download_Sources
{
    print(   "The Linux sources directory is unusable or empty\n\nPlease download sources with `git clone --depth=1 "
           . "https://github.com/raspberrypi/linux`." );

    if ( Response('Download sources now?') ) {
        Git_Sources('https://github.com/raspberrypi/linux');
    }
    else {
        die("Please download valid Linux sources");
    }
}

sub Get_Response
{
    # Grabs user input for a yes/no question.
    print("$_[0] [yes/no]: ");
    chomp( my $response = <<>> );
    return $response;
}

sub Response
{
    Get_Response( $_[0] ) =~ /^ye?s?$/i and return 1;
    return 0;
}

sub Git_Sources
{
    system("git clone --depth=1 $_[0]");
}

### ~~~
# Compile/ Packaging
### ~~~
sub CheckYes
{
    $_[0] =~ /^y(?:e)?(?:s)$/i and return 1;
    print("\nSkipping $_[1]...\n\n") and return 0;
}

sub Compile
{
    # Checks if a config is set to "yes". If true, runs the system command.
    ## EXPECTS: String of user set compiler.
    print("Running command:\t$_[0]\n");
    system("$_[0]");
}

sub Install
{
    # Checks if a config is set to "yes". If true, runs the passed subroutine.
    ## EXPECTS: String of a user config, string containing command for print subroutine reference, list of options
    ##          for the subroutine.
    print("$_[0]\n");
    &{ $_[1] }( $_[2]->@* );
}

sub Compile_Kernel
{
    # Uses the users configuration to create a command, which compiles the kernel for us.
    ## EXPECTS: Reference to YAML hash and Internal hash.

    # Run oldconfig if the user wants it.
    CheckYes( "$_[0]->{'Compile_Time_Options'}->{'Oldconfig'}", 'oldconfig' )
      and Compile("$_[1]->{'Compile'} oldconfig");

    # If we're using menuconfig, run it before we compile the kernel.
    CheckYes( "$_[0]->{'Compile_Time_Options'}->{'Menuconfig'}", 'menuconfig' )
      and Compile("$_[1]->{'Compile'} menuconfig");

    # Compile the kernel.
    CheckYes( "$_[0]->{'Compile_Time_Options'}->{'Kernel'}", 'kernel compilation' )
      and Compile("$_[1]->{'Compile'} zImage modules dtbs");
}

sub Install_Modules
{
    # Installs kernel modules into the temp folder. The headers option build `linux-headers` in usr/lib/modules/<VERSION>/build.
    ## EXPECTS: Reference to YAML hash and Internal hash.
    CheckYes( "$_[0]->{'Packaging_Options'}->{'Install_Modules'}", 'module install' )
      and Install(
        "Installing Modules:\t\t$_[1]->{'Module_Path'}",
        sub {
            system( $_[0] );

            # Remove these files, they're just symlinks to the kernel source. This likely won't exist on the target.
            my $modpath = "$_[1]/lib/modules/$_[2]+";
            unlink("$modpath/build");
            unlink("$modpath/source");
        },
        [ $_[1]->{'Module_Compile'}, $_[1]->{'Module_Path'}, $_[1]->{'Kernel_Version'} ]
      );

    # Because we want a more portable kernel, we have to manually build the headers (/usr/lib/modules/<version>/build).
    # If we were using gentoo or another distro which gives you the kernel source directory, having the build and source
    # symlinks would be fine. For use on most other distros though, they seem to copy only the necessary header files
    # so that third party modules can be built.
    CheckYes( "$_[0]->{'Packaging_Options'}->{'Install_Headers'}", 'header install' )
      and Install(
        "Installing Headers:\t\t$_[1]->{'Module_Path'}/lib/modules/$_[1]->{'Kernel_Version'}+/build",
        sub {
            # Create the build dir, this is where `linux-headers` is found.
            my $builddir = "$_[0]/lib/modules/$_[1]+/build";
            mkpath( "$builddir", 0, 0755 );

            # Copy Makefiles.
            map { copy( "$_", "$builddir" ) } ( 'Makefile', '.config', 'Module.symvers' );
            fcopy( "kernel/Makefile",   "$builddir/kernel/" );
            fcopy( "arch/arm/Makefile", "$builddir/arch/arm/" );

            # Copy over all includes.
            mkpath( "$builddir/include", 0, 0755 );
            map {
                mkpath("$builddir/include/$_");
                dircopy( "include/$_", "$builddir/include/$_" )
            } (
                'acpi',   'asm-generic', 'clocksource', 'config',   'crypto', 'drm',    'dt-bindings', 'generated',
                'keys',   'kvm',         'linux',       'math-emu', 'media',  'memory', 'misc',        'net',
                'pcmcia', 'ras',         'rv',          'scsi',     'soc',    'sound',  'target',      'trace',
                'ufs',    'uapi',        'video',       'xen'
            );

            # Copy arch includes.
            mkpath( "$builddir/arch/arm/include", 0, 0755 );
            dircopy( "arch/arm/include", "$builddir/arch/arm/include" );

            # Remove helper binaries built for host. These are useless on the
            # target system.
            Compile("$_[2] _mrproper_scripts");
            find(
                sub {
                    unlink("$_") if ( "$_" =~ /.*\.o$/ );
                },
                "scripts"
            );

            # Copy over the sanitised scripts dir.
            mkpath( "$builddir/scripts", 0, 0755 );
            dircopy( "scripts", "$builddir/scripts" );

            # Copy over tools include.
            mkpath( "$builddir/tools/include", 0, 0755 );
            dircopy( "tools/include/tools", "$builddir/tools/include/tools" );

            # Copy asm-offsets and vdso
            fcopy( "arch/arm/kernel/asm-offsets.s", "$builddir/arch/arm/kernel/" );
            dircopy( "arch/arm/kernel/vdso", "$builddir/arch/arm/kernel/vdso" );

            # Add dm headers
            map { fcopy( "$_", "$builddir/drivers/md/" ) } ( glob("drivers/md/*.h") );

            # Add inotify
            fcopy( "include/linux/inotify.h", "$builddir/include/linux/" );

            # Add wireless headers
            map { fcopy( "$_", "$builddir/include/linux/" ) } ( glob("net/mac80211/*.h") );

            # Add DVB headers.
            map { fcopy( "$_", "$builddir/include/config/dvb/" ) } ( glob("include/config/dvb/*.h") );
            map { fcopy( "$_", "$builddir/drivers/media/usb/dvb-usb/" ) } ( glob("drivers/media/usb/dvb-usb/*.h") );
            map { fcopy( "$_", "$builddir/drivers/media/dvb-frontends/" ) } ( glob("drivers/media/dvb-frontends/*.h") );
            map { fcopy( "$_", "$builddir/drivers/media/tuners/" ) } ( glob("drivers/media/tuners/*.h") );
            fcopy( "drivers/media/i2c/msp3400-driver.h", "$builddir/drivers/media/i2c/" );

            # Add XFS and shmem for aufs building.
            mkpath( "$builddir/fs/xfs/libxfs", 0, 0755 );
            fcopy( "fs/xfs/libxfs/xfs_sb.h", "$builddir/fs/xfs/libxfs/xfs_sb.h" );
            mkpath( "$builddir/mm", 0, 0755 );

            # Copy Kconfig files.
            find(
                sub {
                    fcopy( "$_", "$builddir/$File::Find::dir/" )
                      if ( "$_" =~ /Kconfig.*/ );
                },
                "."
            );

            # Delete unneeded architectures.
            map { rmtree("$builddir/arch/$_") } (
                'alpha', 'arc',        'arm26',   'arm64',    'avr32',   'blackfin', 'c6x',       'cris',
                'frv',   'h8300',      'hexagon', 'ia64',     'm32r',    'm68k',     'm68knommu', 'metag',
                'mips',  'microblaze', 'mn10300', 'openrisc', 'parisc',  'powerpc',  'ppc',       's390',
                'score', 'sh',         'sh64',    'sparc',    'sparc64', 'tile',     'unicore32', 'um',
                'v850',  'x86',        'xtensa',  'csky',     'nds32',   'nios',     'riscv'
            );

            map { unlink("$builddir/scripts/dtc/include-prefixes/$_") }
              ( 'mips', 'powerpc', 'openrisc', 'microblaze', 'nios2', 'sh', 'arc', 'c6x', 'dt-bindings', 'xtensa', 'arm', 'h8300', 'arm64' );
        },
        [ $_[1]->{'Module_Path'}, $_[1]->{'Kernel_Version'}, $_[1]->{'Compile'} ]
      );
}

sub Install_Boot
{
    # Installs the kernel, dtbs and overlays into the temp/boot folder.
    ## EXPECTS: Reference to YAML hash and Internal hash.
    # Copy the kernel to temp.
    CheckYes( "$_[0]->{'Packaging_Options'}->{'Install_Kernel'}", 'install kernel' )
      and Install(
        "Copying kernel:\t\t\tarch/arm/boot/zImage -> $_[1]->{'Boot_Path'}",
        sub {
            copy( "arch/arm/boot/zImage", "$_[0]" ) or die "Copy failed: $!";
            rename( "$_[0]/zImage", "$_[0]/kernel.img" );
        },
        [ $_[1]->{'Boot_Path'} ]
      );

    # Copy dtb files.
    CheckYes( "$_[0]->{'Packaging_Options'}->{'Install_DTB'}", 'install DTB' )
      and Install(
        "Copying dts:\t\t\tarch/arm/boot/dts/broadcom/*.dtb -> $_[1]->{'Boot_Path'}",
        sub {
            map { copy( "$_", "$_[0]" ) }
            grep { /.*bcm2[78][\d]+\-rpi\-.*/ } glob("arch/arm/boot/dts/broadcom/*.dtb");
        },
        [ $_[1]->{'Boot_Path'} ]
      );

    # Copy the the overlays folder.
    CheckYes( "$_[0]->{'Packaging_Options'}->{'Install_DTS_Overlay'}", 'install DTS overlay' )
      and Install(
        "Copying dts overlay:\t\tarch/arm/boot/overlays -> $_[1]->{'Boot_Path'}/overlays",
        sub {
            dircopy( "arch/arm/boot/dts/overlays", "$_[0]/overlays" ) or die "Copy failed: $!";

            # Remove build artifacts
            find(
                sub {
                    unlink("$_") if ( "$_" =~ /^\..*\.(?:tmp|cmd)$/ );
                },
                "$_[0]/overlays"
            );
        },
        [ $_[1]->{'Boot_Path'} ]
      );

    # Copy the kernel config to boot.
    CheckYes( "$_[0]->{'Packaging_Options'}->{'Install_Config'}", 'install config' )
      and Install(
                   "Copying kernel config:\t\tconfig-$_[1]->{'Kernel_Version'}",
                   sub { copy( ".config", "$_[0]/config-$_[1]" ) },
                   [ $_[1]->{'Boot_Path'}, $_[1]->{'Kernel_Version'} ]
      );

    # Copy System.map
    CheckYes( "$_[0]->{'Packaging_Options'}->{'Install_Syms'}", 'install system.map and symvers' )
      and Install(
        "Copying System.map and symvers:\tSystem.map -> $_[1]->{'Boot_Path'}/System.map\n\t\t\t\tModule.symvers -> "
          . "$_[1]->{'Boot_Path'}/symvers-$_[1]->{'Kernel_Version'}",
        sub {
            copy( "System.map",     "$_[0]" );
            copy( "Module.symvers", "$_[0]/symvers-$_[1]" );
        },
        [ $_[1]->{'Boot_Path'}, $_[1]->{'Kernel_Version'} ]
      );
}

sub Install_Source
{
    # Copys over the kernel source to temp/usr/src/linux-<Kernel-Version>
    ## EXPECTS: Reference to YAML hash and Internal hash.
    my $destSources = "$_[1]->{'Source_Path'}/linux-$_[1]->{'Kernel_Version'}";

    # Moves the linux directory to temp, then deletes all the uneeded git files.
    CheckYes( "$_[0]->{'Packaging_Options'}->{'Install_Kernel_Source'}", 'install kernel source' )
      and Install(
        "Copying Kernel Sources:\t\t$_[1]->{'Linux_Path'} -> $destSources",
        sub {
            dircopy( "$_[0]", "$_[1]" ) or die "Copy failed: $!";
            rmtree("$_[1]/.git");
            rmtree("$_[1]/.github");
            unlink("$_[1]/.gitignore");
            unlink("$_[1]/.gitattributes");
            unlink("$_[1]/.get_maintainer.ignore");
        },
        [ $_[1]->{'Linux_Path'}, $destSources ]
      );
}

sub Tar_Temp
{
    # Turns the temp folder into a tarball and places it into the image directory.
    ## EXPECTS: Reference to YAML hash and Internal Hash..
    chdir( $_[1]->{'Temp_Path'} );

    CheckYes( "$_[0]->{'Packaging_Options'}->{'Tar_Directory'}", 'tar package' )
      and Install(
        "Creating tarball:\t\tlinux-$_[1]->{'Kernel_Version'}.tar.gz",
        sub {
            system("tar -cf - ./* | pv -s \$\(du -cb ./* | tail -1 | awk '{print \$1}'\) | gzip > $_[0]");
            print("Copying file: $_[0] -> $_[1]\n");
            copy( "$_[0]", "$_[1]" ) or die "Copy failed: $!";
        },
        [ "linux-$_[1]->{'Kernel_Version'}-rpi.tar.gz", $_[1]->{'Image_Path'} ]
      );
}

sub Cleanup_Temp
{
    # Delete all directories under the 'Temp' directory..
    ## EXPECTS: Reference to the YAML hash and the Internal Hash.

    # Glob the temp directory so we can grab all the folder names in there (Should be: boot, usr).
    # Remove each globbed folder.
    CheckYes( "$_[0]->{'Cleanup_Options'}->{'Cleanup_Env'}", 'temp directory cleanup' )
      and Install(
        "Cleaning Up:\t\t$_[1]->{'Temp_Path'}",
        sub {
            chdir("$_[0]");
            foreach ( glob("$_[1]/*") ) { rmtree("$_"); }
        },
        [ $_[0]->{'Working_Directories'}->{'Base_Directory'}, $_[1]->{'Temp_Path'} ]
      );

    return $_[0];
}

sub Cleanup_Linux
{
    # Runs `make clean mrproper` in the linux directory then resets the git branch to master.
    ## EXPECTS: Reference to the YAML hash and Internal Hash.
    CheckYes( "$_[0]->{'Cleanup_Options'}->{'Cleanup_Linux'}", 'kernel directory cleanup' )
      and Install(
        "Cleaning Up:\t\t$_[1]->{'Linux_Path'}",
        sub {
            # Copy the .config file, both as config/config and config/config-<version>.
            ( -e "$_[1]" ) and unlink("$_[1]");
            copy( "$_[0]/.config", "$_[1]-$_[2]" );
            copy( "$_[0]/.config", "$_[1]" );

            chdir("$_[0]");
            foreach ( $_[4]->@* ) { system("$_") }
        },
        [
           "$_[1]->{'Linux_Path'}",     "$_[1]->{'Config_Path'}",
           "$_[1]->{'Kernel_Version'}", [ "$_[1]->{'Compile'} clean mrproper", 'git reset --hard', 'git checkout master' ]
        ]
      );
}

### ~~~
### Main Closure and Script Execution
### ~~~
sub Internal_Hash
{
    # Create a hash which 'glues' together information gathered from the YAML config. This contains compile commands
    # and full paths to files which are needed for compiling the kernel.
    my $baseDir = "$_[0]->{'Working_Directories'}->{'Base_Directory'}";
    my $modPath = $baseDir . $_[0]->{'Working_Directories'}->{'Temp_Directory'} . "/usr";
    my $compile =
        "make ARCH=$_[0]->{'Compiler_Options'}->{'Compiler_Arch'} "
      . "CROSS_COMPILE=$_[0]->{'Compiler_Options'}->{'Compiler_Location'} "
      . "-j$_[0]->{'Compiler_Options'}->{'Compiler_Jobs'}";

    # Change to the kernel sources directory. This is to run compiler commands and grab the kernel version.
    chdir("$baseDir$_[0]->{'Working_Directories'}->{'Linux_Directory'}");
    chomp( my $config = `$compile -s kernelversion` );

    # Return relevant information in a hash.
    return {
             'Base_Path'      => $baseDir,
             'Linux_Path'     => $baseDir . $_[0]->{'Working_Directories'}->{'Linux_Directory'},
             'Image_Path'     => $baseDir . $_[0]->{'Working_Directories'}->{'Images_Directory'},
             'Config_Path'    => $baseDir . $_[0]->{'Compile_Time_Options'}->{'Config_Location'},
             'Temp_Path'      => $baseDir . $_[0]->{'Working_Directories'}->{'Temp_Directory'},
             'Module_Path'    => $baseDir . $_[0]->{'Working_Directories'}->{'Temp_Directory'} . '/usr',
             'Boot_Path'      => $baseDir . $_[0]->{'Working_Directories'}->{'Temp_Directory'} . '/boot',
             'Source_Path'    => $baseDir . $_[0]->{'Working_Directories'}->{'Temp_Directory'} . '/usr/src',
             'Compile'        => $compile,
             'Module_Compile' => "$compile INSTALL_MOD_PATH=$modPath modules_install",
             'Kernel_Version' => $config
    };
}

sub Execute
{
    # Create the state for the script. This includes the YAML config and an internal hash which contains compile commands
    # together with full paths to relevant locations.
    my $hashYAML     = Load_Config( shift, shift );
    my $hashInternal = Internal_Hash($hashYAML);

    return sub {
        Check_Build_Env( $hashYAML, $hashInternal );
        Compile_Kernel( $hashYAML, $hashInternal );
        Install_Modules( $hashYAML, $hashInternal );
        Install_Boot( $hashYAML, $hashInternal );
        Install_Source( $hashYAML, $hashInternal );
        Tar_Temp( $hashYAML, $hashInternal );
        Cleanup_Temp( $hashYAML, $hashInternal );
        Cleanup_Linux( $hashYAML, $hashInternal );
    }
}

my $execute = Execute( '/usr/local/etc/', 'pikern.conf' );
$execute->();
